

// Crear un objeto (variable)
//L representa a la bibioteca leaflet
let miMapa = L.map('mapid'); 

//Determinar la vista inicial
miMapa.setView([4.692896,-74.150819], 13);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
miProveedor.addTo(miMapa);


//Crear un objeto marcador
let miMarcador = L.marker([4.690453,-74.147257]);
miMarcador.addTo(miMapa);


//JSON
let circle = L.circle([4.690453,-74.147257], {
    color: 'pink',
    fillColor: 'pink',
    fillOpacity: 0.5,
    radius: 400
});

var polygon = L.polygon([
    [ 4.68953837624876, -74.14668560028076],
    [4.687971861789719, -74.14539009332657],
    [4.689134718422075, -74.14404362440109],
    [4.690171931440072, -74.14542764425278],
    [4.690201336941329, -74.14559125900269],
    [4.690171931440072, -74.14572805166245],
    [4.689541717786085, -74.14669431746006],
    [4.68953837624876, -74.14668560028076]
    
]).addTo(miMapa);

circle.addTo(miMapa);
